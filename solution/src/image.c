#include <image.h>
#include <stdlib.h>

void image_clear(struct image *img) {
	img->height = 0;
	img->width = 0;
	free(img->data);
}

enum init_status image_init(struct image *img, size_t width, size_t height) {
	image_clear(img);
	img->height = height;
	img->width = width;
	img->data = malloc(height * width * sizeof(struct pixel));
	return img->data ? INIT_OK : INIT_BAD_ALLOC;
}

struct pixel* pixel_access(struct pixel *data, size_t index) {
	uint8_t *ptr = (uint8_t*)data;
	ptr += sizeof(struct pixel) * index;
	return (struct pixel*)(ptr);
}

const struct pixel* pixel_access_const(const struct pixel *data, size_t index) {
	const uint8_t *ptr = (const uint8_t*)data;
	ptr += sizeof(struct pixel) * index;
	return (const struct pixel*)(ptr);
}

struct pixel* image_access_pixel(const struct image *img, size_t x, size_t y) {
	if (x >= img->width) return NULL;
	if (y >= img->height) return NULL;
	const size_t index = x + y * img->width;
	return pixel_access(img->data, index);
}

struct image new_image(size_t width, size_t height) {
    struct image res = {0};
    image_init(&res, width, height);
    return res;
}

struct image rotate(struct image const source) {
	struct image res = new_image(source.height, source.width);
	for (size_t i = 0; i < res.height; ++i) {
        for (size_t j = 0; j < res.width; ++j) {
            struct pixel *from = image_access_pixel(&source, res.height-i-1, j);
            struct pixel *to = image_access_pixel(&res, j, i);
            if (from && to) *to = *from;
		}
	}
	return res;
}
