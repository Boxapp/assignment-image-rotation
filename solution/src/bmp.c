#include <bmp.h>
#include <stdlib.h>

const uint16_t BMP_MAGIC = 0x4D42;

struct bmp_header {
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
} __attribute__((packed));

size_t bytes_per_row(size_t width, size_t depth) {
	const size_t bits = width * depth;
	const size_t cells = bits / 32 + (bits % 32 > 0);
	const size_t bytes = cells * 4;
	return bytes;
}

enum read_status from_bmp(FILE* in, struct image* img) {
	if (fseek(in, 0, SEEK_SET)) return READ_BAD_FSEEK;
	struct bmp_header info = {0};
	if (fread(&info, 1, sizeof(info), in) < sizeof(info)) return READ_INVALID_HEADER;
	if (info.bfType != BMP_MAGIC) return READ_INVALID_SIGNATURE;
	const uint32_t width = info.biWidth;
	const uint32_t height = info.biHeight;
	const uint16_t depth = info.biBitCount;
    if (depth != 24) return READ_INVALID_HEADER;
	const size_t row_size = bytes_per_row(width, depth);
    if (info.bOffBits < sizeof(struct bmp_header)) return READ_INVALID_HEADER;
	if (info.bfileSize < info.bOffBits + row_size * height) return READ_INVALID_HEADER;
	if (image_init(img, width, height)) {
		image_clear(img);
		return READ_INIT_FAILED;
	}
	if (fseek(in, info.bOffBits, SEEK_SET)) {
		image_clear(img);
		return READ_BAD_FSEEK;
	}
	struct pixel *row = malloc(row_size);
	if (row == NULL) {
		image_clear(img);
		return READ_BAD_ALLOC;
	}
	for (size_t i = 1; i <= height; ++i) {
		if (fread(row, 1, row_size, in) < width * depth / 8) {
			free(row);
			image_clear(img);
			return READ_INVALID_BITS;
		}
		for (size_t j = 0; j < width; ++j) {
			struct pixel *from = pixel_access(row, j);
			struct pixel *to = image_access_pixel(img, j, height-i);
			if (from && to) *to = *from;
			else {
				free(row);
				image_clear(img);
				return READ_NULL_POINTER;
			}
		}
	}
	free(row);
	return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
	struct bmp_header info = {0};
	info.bfType = BMP_MAGIC;
	info.bOffBits = sizeof(info);
	info.biWidth = img->width;
	info.biHeight = img->height;
	const uint16_t depth = 24;
	info.biBitCount = depth;
	const size_t row_size = bytes_per_row(img->width, depth);
	info.bfileSize = sizeof(info) + row_size * img->height;
	info.biSize = 40;
	info.biPlanes = 1;
	if (fwrite(&info, 1, sizeof(info), out) < sizeof(info)) return WRITE_ERROR;
	struct pixel *row = malloc(row_size);
	if (row == NULL) return WRITE_BAD_ALLOC;
	{
		uint8_t *area = (uint8_t*)row;
		for (size_t i = img->width * depth / 8; i < row_size; ++i) area[i] = 0;
	}
	for (size_t i = 1; i <= img->height; ++i) {
		for (size_t j = 0; j < img->width; ++j) {
			struct pixel *from = image_access_pixel(img, j, img->height-i);
			struct pixel *to = pixel_access(row, j);
			if (from && to) *to = *from;
			else {
				free(row);
				return WRITE_NULL_POINTER;
			}
		}
		if (fwrite(row, 1, row_size, out) < row_size) {
			free(row);
			return WRITE_ERROR;
		}
	}
	free(row);
	return WRITE_OK;
}
