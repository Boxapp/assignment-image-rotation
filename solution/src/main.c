#include <bmp.h>
#include <image.h>
#include <stdio.h>
#include <string.h>

const char *usage = "Usage: %s <input_file> <output_file>\n";
const char *error_opening_file = "%s: error opening file %s\n";
const char *error_reading_image = "%s: error reading image\n";
const char *error_writing_image = "%s: error writing image\n";

enum exit_code {
	OK = 0,
	INVALID_ARGUMENTS,
	ERROR_OPENING_FILE_FOR_READING,
	ERROR_OPENING_FILE_FOR_WRITING,
	ERROR_READING_IMAGE = 10,
	ERROR_WRITING_IMAGE = 20
};

int main(int argc, char **argv) {
	if (argc == 2 && strcmp(argv[1], "--help") == 0) {
		printf(usage, argv[0]);
		return OK;
	}
	if (argc != 3) {
		fprintf(stderr, usage, argv[0]);
		return INVALID_ARGUMENTS;
	}
	FILE *bmp_file = fopen(argv[1], "rb");
	if (bmp_file == NULL) {
		fprintf(stderr, error_opening_file, argv[0], argv[1]);
		return ERROR_OPENING_FILE_FOR_READING;
	}
	struct image bmp = {0};
	int status = from_bmp(bmp_file, &bmp);
	if (status) {
		fprintf(stderr, error_reading_image, argv[0]);
		fclose(bmp_file);
		return ERROR_READING_IMAGE + status;
	}
	fclose(bmp_file);
	struct image res = rotate(bmp);
	image_clear(&bmp);
	FILE *bmp_trans = fopen(argv[2], "wb");
	if (bmp_trans == NULL) {
		fprintf(stderr, error_opening_file, argv[0], argv[2]);
		image_clear(&res);
		return ERROR_OPENING_FILE_FOR_WRITING;
	}
	status = to_bmp(bmp_trans, &res);
	if (status) {
		fprintf(stderr, error_writing_image, argv[0]);
		fclose(bmp_trans);
		image_clear(&res);
		return ERROR_WRITING_IMAGE + status;
	}
	fclose(bmp_trans);
	image_clear(&res);
	return status;
}
