#ifndef __BMP_H__
#define __BMP_H__

#include <image.h>
#include <stddef.h>
#include <stdio.h>

enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_INIT_FAILED,
	READ_BAD_ALLOC,
	READ_BAD_FSEEK,
	READ_NULL_POINTER
};

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR,
	WRITE_BAD_ALLOC,
	WRITE_NULL_POINTER
};

size_t bytes_per_row(size_t width, size_t depth);
enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

#endif
