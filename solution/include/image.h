#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <stddef.h>
#include <stdint.h>

struct pixel {
	uint8_t b, g, r;
};

struct image {
	uint64_t width, height;
	struct pixel* data;
};

enum init_status {
	INIT_OK = 0,
	INIT_BAD_ALLOC
};

void image_clear(struct image *img);
enum init_status image_init(struct image *img, size_t width, size_t height);
struct pixel* pixel_access(struct pixel *data, size_t index);
const struct pixel* pixel_access_const(const struct pixel *data, size_t index);
struct pixel* image_access_pixel(const struct image *img, size_t x, size_t y);
struct image rotate(struct image const source);

#endif
